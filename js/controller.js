app.controller("NavController",function(){
    this.pages = content;
	
    this.toggleActive = function (i) {
        angular.forEach(this.pages, function (p) {
            p.active = false;
        });
        this.pages[i].active = true;
    }
});

app.directive('myDirective', function () {
    return {
        restrict: 'E',
        templateUrl:'sample.html'
    }

});

var content = [
	{
	    title: "Angular Example",
	    description: 'This example is a quick exercise to illustrate how dynamic content can be added to a page. To start the process, lets hit the navbar above',
	    active: true
	},
	{
	    title: "About",
	    description: 'The about description',
	    active: false
	},
	{
	    title: "Contact",
	    description: 'Contact Information',
	    active: false
	},
];